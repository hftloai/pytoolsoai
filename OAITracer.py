"""Python with PyQT implementation of T_Tracer for OpenAirInterface5G project [build of 08/08/2017]
"""
__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "01/2018"
__license__ = ""

import traceback, sys, socket, time
import re
import numpy as np
from copy import deepcopy
from pathlib import Path
# import for loading json files
import json

# import local dependencies
from SimpleLogger import SimpleLogger
from EvalFunctions import *

# common constatns
# default integer size
INT_SIZE = 4
# default unsigned int size
ULONG_SIZE = 8
# default timespec struct size
TIMESPEC_SIZE = 16

class OAITracer:
    # constant
    establishData = ""
    jsonDataFilepath = ""
    tmessagesFilepath = ""
    packageLen = 4
    packageHeaderLen = 20
    initialMessage = "01"
    csvSavePath = {}
    globalMeasStat = {}
    standardMapping = {'frame':int, 'subframe:':int, 'ue_id': int, 'mcs':int, 'alloc':int, 'cshift':int, 'tpc':int}

    # Connect the socket to the port where the server is listening
    serverAddress = ()
    # flags
    exitFlag = False
    established = False
    connected = False

    # parameters
    tRepeatConnection = 2
    attempts = 0
    maxAttempts = 5
    debugPause = 0
    blackList = []
    quiteList = []
    printableList = []

    # variables
    msgGroups = []
    typesCount = 0
    typesCountHex = ""
    cpuFreqMhz = 0.0

    # plot config
    plotOutput = {}
    plotRutimeBuffer = []

    ue_list = {}

    def __init__(self):
        """   """
        self.logger = SimpleLogger()
        # load settings
        self.captureEvents = []
        self.loadSettings("pytracerConfig.json")
        # parse database information
        self.dbData, self.dbDataIds = self.parseDatabase(self.tmessagesFilepath)
        # get number of events
        self.typesCount = len(self.dbData)
        # convert number of events to Hex
        self.typesCountHex = ((self.typesCount).to_bytes(4, byteorder='little')).hex()
        # parse and convert lists
        self.blackList = self.convertList(self.blackList, self.dbDataIds)
        self.quiteList = self.convertList(self.quiteList, self.dbDataIds)
        self.printableList = self.convertList(self.printableList, self.dbDataIds)
        # read is on status of events
        self.readListEnabled(self.jsonDataFilepath)
        # get cpu Freq
        self.cpuFreqMhz = getCpuFreqMHz()
        self.clearCSV()

    def loadSettings(self, filepath):
        data = json.load(open(filepath))
        self.captureEvents = data['events']
        for el in self.captureEvents:
            self.csvSavePath[el['id']] = el['filepath']

        self.jsonDataFilepath = data['settings']['enabledListFilepath']
        self.tmessagesFilepath = data['settings']['tmessagesFilepath']

        self.initialMessage = data['settings']['initialMessage']
        self.serverAddress = (data['settings']['server'], data['settings']['port'])

        self.blackList = data['settings']['blackList']
        self.quiteList = data['settings']['quiteList']
        self.printableList = data['settings']['printableList']

    def clearCSV(self):
        """Remove all measurement results in the folder
        """
        for f in self.csvSavePath:
            checkFile = Path(self.csvSavePath[f])
            if checkFile.is_file():
                checkFile.unlink()

    def parseMeasurementSpecLog(self, pTime, pType, frameData, type, paramsToSave=[], logType="LEGACY_MAC_INFO"):
        if len(type) < 1 or len(paramsToSave) < 1 or len(logType) < 1:
            # TODO: add log messages to this wrong parameters
            return -1            
        if not type in self.globalMeasStat:
            self.globalMeasStat[type] = {'headerWFlag': False}
        filePath = self.csvSavePath[type] if type in self.csvSavePath else 'temp.csv'                            
        if self.dbData[pType]["ID"] == logType and frameData[0].find(type) >= 0:
            dataToSave = frameData[0].split("|")
            dataToSaveObj = {}
            csvOutput = [pTime]
            if not self.globalMeasStat[type]['headerWFlag']:
                csvHeader = ['timestamp']
            for i in range(1,len(dataToSave)):
                tmp = dataToSave[i].split(" ")
                if tmp[0] in paramsToSave:
                    mapFunc = self.standardMapping[tmp[0]] if tmp[0] in self.standardMapping else str
                    if not self.globalMeasStat[type]['headerWFlag'] and not tmp[0] in csvHeader:
                        csvHeader.append(tmp[0])
                    csvOutput.append(mapFunc(tmp[1]))
            # write header only once
            if not self.globalMeasStat[type]['headerWFlag']:
                appendToCSV(csvHeader, filePath)
                self.globalMeasStat[type]['headerWFlag'] = True
            appendToCSV(csvOutput, filePath)        

    def plotInit(self, plotId, plotRef, plotTitle = "Untitled plot"):
        """Initialize plot object
        
        Arguments:
            plotId {string} -- plot id
            plotRef {string} -- plot reference
        """
        self.plotOutput[plotId] = {}
        self.plotOutput[plotId]["title"] = plotTitle
        self.plotOutput[plotId]["ref"] = plotRef
        self.plotOutput[plotId]["X"] = []
        self.plotOutput[plotId]["Y"] = []

    def plotResetValues(self, plotId):
        self.plotOutput[plotId]["X"] = []
        self.plotOutput[plotId]["Y"] = []

    def plotGetObj(self, plotId):
        return self.plotOutput[plotId]

    def plotExists(self, plotId):
        return (plotId in self.plotOutput)

    def plotAddValue(self, plotId, plotValue, plotKey=None):
        """ adds value to the plot """
        if not plotId in self.plotOutput:
            self.plotInit(plotId, plotId)
        self.plotOutput[plotId]["Y"].append(plotValue)
        self.plotOutput[plotId]["X"].append(plotKey if not plotKey is None else len(self.plotOutput[plotId]["Y"])-1)
        return len(self.plotOutput[plotId]["Y"])

    def plotAddBuffer(self, plotId, plotBuffer):
        """ adds buffer to the plot """
        # TODO: add buffers
        pass

    def plotRuntime(self, data, key=None):
        """ plot runtime graph """
        defPlotId = "runtimeMeas"
        if not self.plotExists(defPlotId):
            self.plotInit(defPlotId, "Runtime", "Runtime plot")
        vals = self.plotAddValue(defPlotId, data / self.cpuFreqMhz, key)
        # self.plotRutimeBuffer.append(dataMs)
        if vals == 10:
            self.emitSignal("updateRuntimePlot", deepcopy(self.plotGetObj(defPlotId)))
            self.plotResetValues(defPlotId)

    def plotResources(self, data, key=None):
        """ plot runtime graph """
        defPlotId = "runtimeMeas"
        if not self.plotExists(defPlotId):
            self.plotInit(defPlotId, "Resources", "Resources plot")
        vals = self.plotAddValue(defPlotId, data, key)
        # self.plotRutimeBuffer.append(dataMs)
        if vals == 10:
            self.emitSignal("updateResourcePlot", deepcopy(self.plotGetObj(defPlotId)))
            self.plotResetValues(defPlotId)

    def getFullPackage(self, pSize):
        """ reads full package with size pSize from socket  """
        return self.sock.recv(pSize)

    def getPackage(self, pSize):
        """ gets package with size pSize from the socket """
        fullPackage = self.getFullPackage(pSize)
        # package header
        header = fullPackage[:self.packageHeaderLen]
        pTime = unpackTimespec(header[:TIMESPEC_SIZE])
        # package content
        data = fullPackage[self.packageHeaderLen:]
        # package typet =
        pType = int.from_bytes(header[-INT_SIZE:], byteorder=sys.byteorder, signed=True)
        return (pTime, pType, data)

    def getPackageLen(self, pSize):
        """ gets first pSize bytes with the size of package  """
        data = self.getFullPackage(pSize)
        dataLen = int.from_bytes(data, byteorder=sys.byteorder)
        return dataLen

    def sendPackage(self, hexData):
        """ sends full package of hex data """
        return self.sock.send(bytes.fromhex(hexData))

    def readListEnabled(self, filename):
        """ reads json file saying which events should be used """
        establishData = json.load(open(filename))
        for i in range(self.typesCount):
            # print(i, self.dbData[i]["ID"], establishData[str(i)])
            self.establishData += "01000000" if establishData[str(i)] else "00000000"
        return self.establishData

    def parseDatabase(self, filename):
        """ parses T_Messages.txt database file """
        # /home/epc1/dev/T-tracer/T_messages.txt
        database = []
        indexDatabase = []
        f = open(filename, "r")
        objPos = 0
        for line in f:
            if len(line) > 0:
                line = line.strip()
                if objPos > 0:
                    if objPos == 1:
                        objDesc = line[7:]
                        objPos += 1
                    elif objPos == 2:
                        objGroup = line[8:]
                        if not objGroup in self.msgGroups:
                            self.msgGroups.append(objGroup)
                        objPos += 1
                    elif objPos == 3:
                        objPos = 0
                        objFormat = [(f.split(",")[0], f.split(",")[1]) if len(f.split(","))>1 else f for f in ("".join(line[9:].split())).split(":")]
                        database.append({"ID":objId, "DESC":objDesc, "GROUP":objGroup, "FORMAT":objFormat})
                        indexDatabase.append(objId)
                if line[0:2] == "ID":
                    objPos = 1
                    objId = line[5:]
        f.close()
        return database, indexDatabase

    def convertList(self, listData, dbIndexData):
        """ converts string list of regex to index list """
        result = []
        # compile regular expressions
        printableListPatterns = [re.compile(p) for p in listData]
        # go over the database
        for el in range(len(self.dbData)):
            occurCnt = sum([pPattern.match(self.dbData[el]["ID"]) is not None for pPattern in printableListPatterns])
            if occurCnt > 0:
                result.append(el)
        return result

    def parseFrame(self, pType, pContent):
        """ Parse from coming from the LTEsoftmodem """
        result = []
        if pType < 0:
            #print("System information received, type = %d" % pType)
            return "System information", []
        if pType > self.typesCount - 1:
            #print("Wrong frame type received = %d" % pType)
            return "System information", []
        frameFormats = self.dbData[pType]["FORMAT"]
        frameType = self.dbData[pType]["ID"]
        pOffset = 0
        for frameFormat, frameFormatDesc in frameFormats:
            if pOffset ==len(pContent):
                break

            if frameFormat == "int":
                res = int.from_bytes(pContent[pOffset:pOffset+4], byteorder=sys.byteorder, signed=True)
                result.append(res)
                pOffset += INT_SIZE
            elif frameFormat == "ulong":
                res = int.from_bytes(pContent[pOffset:pOffset+8], byteorder=sys.byteorder)
                result.append(res)
                pOffset += ULONG_SIZE
            elif frameFormat == "string":
                # print(dbData[pType], pType)
                result.append(pContent[pOffset:].decode("utf-8", "ignore").strip('\0').strip())
                pOffset = len(pContent)
            elif frameFormat == "buffer":
                # get buffer length
                bufLen = int.from_bytes(pContent[pOffset:pOffset+4], byteorder=sys.byteorder, signed=True)
                pOffset += INT_SIZE
                # result.append(pContent[pOffset:].decode("utf-8", "ignore").strip('\0').strip())
                # pOffset = len(pContent)
            #else:
            #    print(frameFormat)
        #print(dbData[pType] + ": "+ pContent.decode("utf-8", "ignore").strip('\0').strip())
        return frameType, result

    def emitSignal(self, signal = None, *params):
        if signal is None or self.signals is None:
            return False
        signalingObj = getattr(self.signals, signal)
        signalingObj.emit(*params)

    def mainLoop(self, signals = None):
        """ main procedure, infinite loop here """
        # deine QT signals
        self.signals = signals
        # set default falues to flags
        self.exitFlag = False
        self.connected = False
        self.established = False
        # reset counter for number of attempts
        self.attempts = 0
        # Create a TCP/IP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # run the main loop
        while (not self.exitFlag):
            # if not connected yet and attemts are not exceeded
            while (not self.connected and self.attempts <= self.maxAttempts):
                try:
                    if self.attempts > 0:
                        time.sleep(self.tRepeatConnection)
                    self.logger.LOG("Connecting to %s on port %s"% (self.serverAddress[0], self.serverAddress[1]))
                    # update current status in QT
                    self.emitSignal("updateConnectionInfo")

                    self.sock.connect(self.serverAddress)
                    self.connected = True
                    self.attempts = 0
                except:
                    self.attempts += 1
                    self.logger.LOG("Connection refused (%d of %d), repeat in %d s" % (self.attempts, self.maxAttempts, self.tRepeatConnection), self.logger.SEVERITY_INFO)
            # update current status in QT
            self.emitSignal("updateConnectionInfo")

            # abort if not connected
            if not self.connected:
                self.exitFlag = True
                break

            # establish connection if not yet
            if not self.established:
                if self.sendPackage(self.initialMessage) > 0 and self.sendPackage(self.typesCountHex) > 0 and self.sendPackage(self.establishData) > 0:
                    self.established = True
                else:
                    pass
            else:
                receivedSize = self.getPackageLen(self.packageLen)
                if receivedSize > 0:
                    pTime, pType, pContent = self.getPackage(receivedSize)
                    if not pType in self.blackList:
                        if pType <= 0 or pType >= self.typesCount:
                            continue
                        frameType, frameData = self.parseFrame(pType, pContent)
                        if self.dbData[pType]["ID"] == "LEGACY_RUNTIMEMEAS_INFO":
                            self.plotRuntime(float(frameData[0]))
                            appendToCSV([pTime, float(frameData[0]) / self.cpuFreqMhz])
                        for event in self.captureEvents:
                            self.parseMeasurementSpecLog(pTime, pType, frameData, event["id"], event["vars"])

                        # log event if in prinable list and not muted
                        if pType in self.printableList and not pType in self.quiteList:
                            # use logger
                            self.logger.LOG(str(frameType) + ": " + str(frameData), self.logger.SEVERITY_INFO)
                            # forward log to the QT
                            self.emitSignal("addToLog", formatTimestampToString(pTime) + ": " + str(frameType) + ": " + str(frameData))
                        # pause if in debug mode
                        if self.debugPause > 0:
                            time.sleep(self.debugPause)
