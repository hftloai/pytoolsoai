"""
"""

__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "01/2018"
__license__ = ""

import numpy as np
import csv
import os
import glob
import matplotlib.pyplot as plt

def readFile(filename="test.csv", nValues=1000):
    xValues = []
    yValues = []
    with open(filename, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        i = 0
        for row in spamreader:
            if i == nValues:
                break
            i += 1
            xValues.append(float(row[0]))
            yValues.append(float(row[1]))
    return xValues, yValues

def plotCdf(yValues, lbl="", isCcdf=False):
    yValues = np.array(yValues)
    # sort values
    yValues = np.sort(yValues)
    # normalize values to the sum of all
    yValuesNorm = yValues/ sum(yValues)
    # calc the cumulative sum
    cY = np.cumsum(yValuesNorm)
    # if plot a CCDF
    if isCcdf:
        cY = 1 - cY
    # prepare the plot
    plt.plot(yValues, cY, label=lbl)
    plt.xlabel(u'Execution time [\u00b5s]')
    plt.ylabel('CCDF' if isCcdf else 'CDF')
    plt.grid(True)
    return plt

def plotLine(yValues, lbl='', xValues=None):
    if xValues is None:
        xValues = list(range(1, len(yValues)+1))
    plt.plot(xValues, yValues, 'ro')
    plt.grid(True) 
    return plt

def calcDiffValues(xValues):
    # convert to ms
    xValues = np.sort(np.array(xValues) * 1000)
    return xValues[1:] - xValues[:-1]

def plotDiffVals(xValues, lbl=""):
    plt.xlabel('Call time difference [ms]')
    plt.ylabel('CDF')
    plt.grid(True)

if __name__ == "__main__":
    pass