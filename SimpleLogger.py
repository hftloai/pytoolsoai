"""Simple logger functionality
"""

__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "01/2018"
__license__ = ""

class SimpleLogger:
    """ simple logger implementation with severity level support"""
    SEVERITY_FATAL = 0
    SEVERITY_ERROR = 3
    SEVERITY_WARNING = 5
    SEVERITY_INFO = 10
    SEVERITY_DEBUG = 15

    severityLevelToStr = {
        SEVERITY_FATAL: 'FATAL',
        SEVERITY_ERROR: 'ERROR',
        SEVERITY_WARNING: 'WARNING',
        SEVERITY_INFO: 'INFO',
        SEVERITY_DEBUG: 'DEBUG'
    }
    enabledFlag = True
    severityLevel = 0

    def __init__(self, enabledFlag = True, severityLevel = SEVERITY_WARNING):
        """ init logger """
        self.enabledFlag = enabledFlag
        self.severityLevel = severityLevel

    def LOG(self, message, severityLevel=SEVERITY_INFO):
        """ print log message """
        if severityLevel > self.severityLevel:
            return False
        print("[LOG][%s] %s" % (self.severityLevelToStr[severityLevel], message))
