"""LTE RB allocation visualization tool
"""

__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "01/2018"
__license__ = ""

from EvalFunctions import *

import sys
import os
import matplotlib.pyplot as plt
import numpy as np
from  SimpleLogger import SimpleLogger

class Parser:
    # File types, UL and DL channel results
    RB_ALLOC_UL = 1
    RB_ALLOC_DL = 0
    # default number of subframes
    N_SUBFRAMES = 10
    # default number of resource block groups, this is 25 RBs (5 MHz channel)
    N_RBG = 13
    # default number of RB in uplink, this is 25 RBs (5 MHz channel)
    N_RB_UL = 25
    #
    AVAILABLE_PARAMS_TO_PLOT = ['nrb', 'mcs']
    #
    PARAM_DESCRIPTION = {}


    def __init__(self, nSamples=None):
        self.PARAM_DESCRIPTION = {
            self.RB_ALLOC_UL: {
                'nrb': {
                    'SHORT': '# RBs',
                    'FULL': 'resource block(s)',
                    'TOTAL_FUNC': sum,
                    'PLOT_TOTAL': True
                },        
                'mcs': {
                    'SHORT': 'MCS',
                    'FULL': 'modulation coding scheme',
                    'PLOT_TOTAL': False,
                    'TOTAL_FUNC': max,
                },
            },
            self.RB_ALLOC_DL: {
                'nrb': {
                    'SHORT': '# RBGs',
                    'FULL': 'resource block group(s)',
                    'PLOT_TOTAL': True,
                    'TOTAL_FUNC': sum,
                },        
                'mcs': {
                    'SHORT': 'MCS',
                    'FULL': 'modulation coding scheme',
                    'PLOT_TOTAL': False,
                    'TOTAL_FUNC': max,
                },        
            }
        }        
        # Connect logger
        self.logger = SimpleLogger()
        # default header for the DL channel
        self.header = ["timestamp", "frame", "subframe", "ue_id", "mcs", "alloc"] # , "cshift", "tpc"]
        # default mapping functions for a DL channel
        self.mappingFunc = [float, int, int, int, int ,int, int, int]
        # initialize parsed dict
        self.parsed = {}
        # initialize the path to the file
        self.filePath = ""
        # define number of CSV file lines to be processed
        self.nSamplesInitial = nSamples
        self.nSamples = self.nSamplesInitial
        # UL or DL scheduling info RB_ALLOC_UL or RB_ALLOC_DL
        self.fileType = None
        # number of UE found in the CSV file
        self.maxUECount = 0
        self.UEList = set([])
        self.linesCountTotal = 0
        self.linesCountRead = 0

    def loadFile(self, filePath):
        """Load a CSV file, determine content type
        
        Arguments:
            filename {string} -- path to file
        """
        self.filePath = filePath
        self.linesCountTotal = linesCount(self.filePath)
        if self.nSamplesInitial is None or self.nSamplesInitial > self.linesCountTotal:
            self.nSamples = self.linesCountTotal
        else:
            self.nSamples = self.nSamplesInitial

        res, self.header = readCSVFile(self.filePath, self.nSamples, self.header, int, self.mappingFunc)
        self.fileType = self.RB_ALLOC_UL if 'tpc' in self.header else self.RB_ALLOC_DL
        self.parsed = self.parseResult(res)
        self.linesCountRead += int(len(self.header) > 0)
        self.filename = os.path.basename(self.filePath)

    def parseResult(self, result):
        """Process CSV input to a structured format
        
        Arguments:
            result {list of dictionaries} -- input CSV parsed to the table
        
        Returns:
            dictionary -- structured data
        """
        self.linesCountRead = 0
        # init UE counter
        self.UEList = set([])
        self.maxUECount = 0
        # init result dict
        parsedResult = {}
        # last frame number
        lastFrame = -1
        # iteration over the frames, since the frame number repeats over time
        i = -1
        # loop over the CSV file lines
        for r in result:
            self.linesCountRead += 1
            # TODO: add check that more than 10 subframes in a frame found
            # if the next frame found or reached the maximum number of subframe by specification
            if lastFrame != r["frame"]:
                if i > -1:
                    frameNrbRatio = parsedResult[i]['nrb'] / ((self.N_RB_UL if self.fileType == self.RB_ALLOC_UL else self.N_RBG)*self.N_SUBFRAMES)
                    parsedResult[i]['nrb_ratio'] = frameNrbRatio
                i += 1
                lastFrame = r["frame"]
                parsedResult[i] = {"frame": r['frame'], 'nrb':0, 'nrb_ratio':0, "subframes": {}}
            # if found a new subframe
            if not r['subframe'] in parsedResult[i]["subframes"]:
                parsedResult[i]["subframes"][r['subframe']] = {}
            # calculate number of RBs and allocation bitmap
            if self.fileType == self.RB_ALLOC_DL:
                # assume simple allocation for RBG
                alloc = np.array(convertIntToBinIntArray(r['alloc'], self.N_RBG))
                NRB = sum(alloc)
                nrbRatio = NRB/ self.N_RBG
            else:
                # for UL calculate from RIV
                alloc = np.array(rivToBitmapArray(r['alloc'],self.N_RB_UL))
                _, NRB, _ = unpackRiv(r['alloc'],self.N_RB_UL)
                nrbRatio = NRB/ self.N_RB_UL
            parsedResult[i]['nrb'] += NRB 
            self.UEList.add(r['ue_id'])
            # store the subframe
            parsedResult[i]["subframes"][r['subframe']][r['ue_id']] = {'timestamp':r['timestamp'], 'mcs':r['mcs'], 'alloc':alloc, 'nrb': NRB, 'nrb_ratio':nrbRatio}
        self.maxUECount = len(self.UEList)
        return parsedResult   

    def getFrameDistribution(self, param = 'nrb'):
        """Get simple frame distribution for number of allocated RB(G)s

        """
        maxResult = 0
        if self.maxUECount > 1 and self.PARAM_DESCRIPTION[self.fileType][param]['PLOT_TOTAL']:
            result = [[] for i in range(self.maxUECount + 1)]
        else:
            result = [[] for i in range(self.maxUECount)]

        for frame in self.parsed:
            if self.maxUECount > 1 and self.PARAM_DESCRIPTION[self.fileType][param]['PLOT_TOTAL']:
                subresult = [0 for i in range(self.maxUECount + 1)]
            else:
                subresult = [0 for i in range(self.maxUECount)]

            for subframe in self.parsed[frame]['subframes']:
                total = 0
                for ueId in self.UEList:
                    val = 0 if not ueId in self.parsed[frame]['subframes'][subframe] else  self.parsed[frame]['subframes'][subframe][ueId][param]
                    # total += val 
                    total = self.PARAM_DESCRIPTION[self.fileType][param]['TOTAL_FUNC']([total, val])
                    subresult[ueId] = self.PARAM_DESCRIPTION[self.fileType][param]['TOTAL_FUNC']([subresult[ueId], val])
                    # subresult[ueId] += val
                if self.maxUECount > 1 and self.PARAM_DESCRIPTION[self.fileType][param]['PLOT_TOTAL']:
                    subresult[self.maxUECount] = self.PARAM_DESCRIPTION[self.fileType][param]['TOTAL_FUNC']([subresult[self.maxUECount], total])
            for i in range(len(subresult)):
                maxResult = max(maxResult, subresult[i])
                result[i].append(subresult[i])
            
        return result, maxResult            

    def getSubframeDistribution(self, param = 'nrb'):
        """Get simple subframe distribution for number of allocated RB(G)s

        """
        maxResult = 0
        if self.maxUECount > 1 and self.PARAM_DESCRIPTION[self.fileType][param]['PLOT_TOTAL']:
            result = [[] for i in range(self.maxUECount + 1)]
        else:
            result = [[] for i in range(self.maxUECount)]
        for frame in self.parsed:
            for subframe in self.parsed[frame]['subframes']:
                total = 0
                for ueId in self.UEList:
                    val = 0 if not ueId in self.parsed[frame]['subframes'][subframe] else  self.parsed[frame]['subframes'][subframe][ueId][param]
                    # total += val 
                    total = self.PARAM_DESCRIPTION[self.fileType][param]['TOTAL_FUNC']([total, val])
                    result[ueId].append(val)
                maxResult = max(maxResult, total)
                if self.maxUECount > 1 and self.PARAM_DESCRIPTION[self.fileType][param]['PLOT_TOTAL']:
                    result[self.maxUECount].append(total)
        return result, maxResult

    def getTimeDistribution(self, param = 'nrb'):
        """Get simple time distribution for number of allocated RB(G)s

        """
        # init values dicts 
        times = {}
                    
        values = {}
        # init starting time stamp
        startTime = None
        for frame in self.parsed:
            for subframe in self.parsed[frame]['subframes']:
                for ueId in self.parsed[frame]['subframes'][subframe]:
                    # init if the first value
                    if ueId not in times:
                        times[ueId] = []
                        values[ueId] = []
                    currentTime = self.parsed[frame]['subframes'][subframe][ueId]['timestamp']
                    startTime = currentTime if startTime is None or currentTime < startTime else startTime
                    times[ueId].append(self.parsed[frame]['subframes'][subframe][ueId]['timestamp'])
                    values[ueId].append(self.parsed[frame]['subframes'][subframe][ueId][param])
        for i in times:
            times[i] = np.array(times[i]) - startTime
        return times, values, startTime

    def plotSubframeDistribution(self, subplot, param='nrb'):
        """Plot simple time distribution for number of allocated RB(G)s as a colormap

        """
        distribution, maxCBValue = self.getSubframeDistribution(param)

        subplot.clf()
        splt = subplot.add_subplot(111)

        im = splt.pcolor(distribution, cmap='Greys', vmin=.1)

        # maxCBValue = (self.N_RB_UL if self.fileType == RB_ALLOC_UL else self.N_RBG)*N_SUBFRAMES
        # TODO: add proper ticks = range(0 , maxCBValue, 10) ?
        colorbar = subplot.colorbar(im, boundaries = range(maxCBValue+1), spacing = 'proportional')
        colorbar.ax.set_xlabel(self.PARAM_DESCRIPTION[self.fileType][param]['SHORT'])
        colorbar.ax.xaxis.set_label_position('top')

        splt.set_title("Subframes distribution for " + self.PARAM_DESCRIPTION[self.fileType][param]['FULL'])
        splt.set_ylabel('UE id')
        splt.set_xlabel('Subframe counter')

        row_labels = [str(i) for i in range(self.maxUECount)]
        if self.maxUECount > 1 and self.PARAM_DESCRIPTION[self.fileType][param]['PLOT_TOTAL']:
            row_labels.append("Total")
            splt.set_yticks(range(self.maxUECount+1),minor=False)
            splt.set_yticklabels(row_labels)
        else:
            splt.set_yticks(range(self.maxUECount),minor=False)

    def plotFrameDistribution(self, subplot, param='nrb'):
        """Plot simple time distribution for number of allocated RB(G)s as a colormap

        """
        distribution, maxCBValue = self.getFrameDistribution(param)

        subplot.clf()
        splt = subplot.add_subplot(111)

        im = splt.pcolor(distribution, cmap='Greys', vmin=.1)

        # maxCBValue = (self.N_RB_UL if self.fileType == RB_ALLOC_UL else self.N_RBG)*N_SUBFRAMES
        # TODO: add proper ticks = range(0 , maxCBValue, 10) ?
        colorbar = subplot.colorbar(im, boundaries = range(maxCBValue+1), spacing = 'proportional')
        colorbar.ax.set_xlabel(self.PARAM_DESCRIPTION[self.fileType][param]['SHORT'])
        colorbar.ax.xaxis.set_label_position('top')

        splt.set_title("Frame distribution for " + self.PARAM_DESCRIPTION[self.fileType][param]['FULL'])
        splt.set_ylabel('UE id')
        splt.set_xlabel('Frame counter')

        row_labels = [str(i) for i in range(self.maxUECount)]
        if self.maxUECount > 1 and self.PARAM_DESCRIPTION[self.fileType][param]['PLOT_TOTAL']:
            row_labels.append("Total")
            splt.set_yticks(range(self.maxUECount+1),minor=False)
            splt.set_yticklabels(row_labels)
        else:
            splt.set_yticks(range(self.maxUECount),minor=False)                     

    def plotCDFTimeDistribution(self, subplot, param='nrb'):
        """Plot CDF for time distribution for number of allocated RB(G)s
        """
        times, values, _ = self.getTimeDistribution(param)

        subplot.clf()
        splt = subplot.add_subplot(111)
        maxVal = 0
        for i in times:
            vals, cValues = calcCdfOrCcdf(values[i])
            maxVal = max(maxVal, max(vals))
            splt.plot(vals, cValues, label="UE"+str(i))
        splt.set_title("CDF for " + self.PARAM_DESCRIPTION[self.fileType][param]['FULL'])
        splt.set_xlabel(self.PARAM_DESCRIPTION[self.fileType][param]['FULL'])
        splt.set_ylabel('CDF')
        # add the legend in the middle of the plot
        leg = splt.legend(fancybox=True, loc='lower right')
        # set the alpha value of the legend: it will be translucent
        leg.get_frame().set_alpha(0.5)
        #set x and y ticks and labels
        splt.set_xticks(range(maxVal),minor=False)   
        # enable grid
        splt.grid(True)

    def plotTimeDistribution(self, subplot, param='nrb'):
        """Plot simple time distribution for number of allocated RB(G)s
        
        """
        times, values, _ = self.getTimeDistribution(param)

        subplot.clf()
        splt = subplot.add_subplot(111)
        for i in times:
            splt.plot(times[i], values[i], label="UE"+str(i))
        splt.set_title("Time distribution for " + self.PARAM_DESCRIPTION[self.fileType][param]['FULL'])
        splt.set_ylabel(self.PARAM_DESCRIPTION[self.fileType][param]['FULL'])
        splt.set_xlabel('Time after the first subframe')            
        # add the legend in the middle of the plot
        leg = splt.legend(fancybox=True, loc='lower right')
        # set the alpha value of the legend: it will be translucent
        leg.get_frame().set_alpha(0.5)        
        splt.grid(True)
        plt.show()

    def plotFrame(self, frameN, subplot):
        """Plot the resource block / resource block group allocation
        
        Arguments:
            frameN {integer} -- frame id
            subplot {object} -- matplotlib object for the subplot
        """
        subplot.clf()
        splt = subplot.add_subplot(111)
        # plt.figure(figsize=(5,5))
        frame = self.parsed[frameN]
        nrows, ncols = self.N_SUBFRAMES, self.N_RBG if self.fileType == self.RB_ALLOC_DL else self.N_RB_UL
        image = np.zeros((nrows, ncols))
        max_ue = 0
        for subFrame in frame["subframes"]:
            allocs = np.zeros(ncols)
            for ue in frame["subframes"][subFrame]:
                alloc = frame["subframes"][subFrame][ue]["alloc"]
                allocs += alloc*(ue+1)
                for i in range(ncols):
                    if alloc[i] > 0:
                        splt.text(i, subFrame, str(frame["subframes"][subFrame][ue]["mcs"]), va='center', ha='center', color='white')
            image[subFrame] = allocs
            max_ue = max(allocs) if max_ue < max(allocs) else max_ue
        max_ue = int(max_ue)
        row_labels = range(nrows)
        col_labels = [str(i) for i in range(1,ncols+1)]
        im = splt.matshow(image, cmap='Greys',interpolation="nearest")

        colorbar = subplot.colorbar(im, ticks = range(1,max_ue+1), boundaries = range(0,max_ue+2), spacing = 'proportional',)
        colorbar.ax.set_xlabel('UE id')
        colorbar.ax.xaxis.set_label_position('top')
        # set titles
        splt.set_title("RB allocation for the frame %d" % frame["frame"], y=1.08)
        splt.set_xlabel('# of Resource block group' if self.fileType == self.RB_ALLOC_DL else "# of Resource block")
        splt.set_ylabel('Sub-frame')

        #set x and y ticks and labels
        splt.set_xticks(range(ncols),minor=False)
        splt.set_xticklabels(col_labels)
        splt.set_yticks(range(nrows),minor=False)
        splt.set_yticklabels(row_labels)

        #set minor axes in between the labels
        splt.set_xticks([x-0.5 for x in range(1,ncols)],minor=True )
        splt.set_yticks([y-0.5 for y in range(1,nrows)],minor=True)
        #plot grid on minor axes
        splt.grid(which="minor",ls="-",lw=2)
        # plt.show()

if __name__ == "__main__":
    # TODO: add a console enabled stuff
    pass
