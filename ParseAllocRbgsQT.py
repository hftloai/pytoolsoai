"""GUI interface for the LTE RB allocation visualization tool

Tested with QT 5.8
"""

__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "01/2018"
__license__ = ""

from ParseAllocRbgs import Parser

import sys

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from PyQt5.QtWidgets import QVBoxLayout, QSizePolicy, QMenu, QAction
from PyQt5 import QtGui, QtCore, QtWidgets

class PlotCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
 
        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

class Window(QtWidgets.QWidget):
    col_list = ["FRAME", "SubFrames", "UEs", "Alloc ratio"]

    def __init__(self):
        QtWidgets.QWidget.__init__(self)

        self.layoutCols = 4
        self.layoutTableRows = 7

        self.plotCanvas = PlotCanvas(self, width=5, height=4)
        self.figure = self.plotCanvas.figure

        self.parser = Parser()

        self.setWindowTitle(self.tr("RBG allocation visualization"))
        # setup buttons
        self.saveButton = QtWidgets.QPushButton(self.tr("Save &Plot"))
        self.cdfTimePlotButton = QtWidgets.QPushButton(self.tr("Plot CDF"))
        self.subframePlotButton = QtWidgets.QPushButton(self.tr("Plot subframe distribution"))
        self.framePlotButton = QtWidgets.QPushButton(self.tr("Plot frame distribution"))
        self.loadButton = QtWidgets.QPushButton(self.tr("&Load"))
        # setup labels
        self.lblFileInfo = QtWidgets.QLabel(self.tr("File: {load file first}\nng"))
        self.lblFileInfo.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        # setup table
        self.table = QtWidgets.QTableWidget(1, len(self.col_list), self)
        for i in range(len(self.col_list)):
            self.table.setHorizontalHeaderItem(i, QtWidgets.QTableWidgetItem(self.col_list[i]))
        # set column width
        self.table.setColumnWidth(0,300)
        self.table.setColumnWidth(1,200)
        self.table.setColumnWidth(2,200)
        # bind actions
        self.table.currentItemChanged.connect(self.handleItemClicked)
        self.table.itemClicked.connect(self.handleItemClicked)
        self.subframePlotButton.pressed.connect(self.subframePlot)
        self.framePlotButton.pressed.connect(self.framePlot)
        self.cdfTimePlotButton.pressed.connect(self.cdfTimePlot)
        self.saveButton.pressed.connect(self.savePlot)
        self.loadButton.pressed.connect(self.fillTable)
        # prepare layout
        self.layout = QtWidgets.QGridLayout(self)
        self.layout.addWidget(self.table, 0, 0, self.layoutTableRows, self.layoutCols-1)
        self.layout.addWidget(self.loadButton, 0, 3)
        self.layout.addWidget(self.subframePlotButton, 1, 3) 
        self.layout.addWidget(self.framePlotButton, 2, 3) 
        self.layout.addWidget(self.cdfTimePlotButton, 3, 3)
        self.layout.addWidget(self.saveButton, 4, 3)

        self.layout.addWidget(self.lblFileInfo, 5, 3)

        self.layout.addWidget(self.plotCanvas, self.layoutTableRows, 0, 2, self.layoutCols)  
        # bind context menu
        self.subframePlotButton.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.subframePlotButton.customContextMenuRequested.connect(self.onSubframePlotContext)
        self.framePlotButton.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.framePlotButton.customContextMenuRequested.connect(self.onFramePlotContext)        
        self.cdfTimePlotButton.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.cdfTimePlotButton.customContextMenuRequested.connect(self.onCDFlotContext)        

    def onCDFlotContext(self, point):
        actions = []
        descActions = []

        menu = QMenu(self)
        for el in self.parser.AVAILABLE_PARAMS_TO_PLOT:
            newAction = menu.addAction(self.parser.PARAM_DESCRIPTION[self.parser.fileType][el]['SHORT'])
            actions.append(newAction)
            descActions.append(el)
        action = menu.exec_(self.cdfTimePlotButton.mapToGlobal(point))
        
        if action in actions:
            self.cdfTimePlot(descActions[actions.index(action)])

    def onSubframePlotContext(self, point):
        actions = []
        descActions = []

        menu = QMenu(self)
        for el in self.parser.AVAILABLE_PARAMS_TO_PLOT:
            newAction = menu.addAction(self.parser.PARAM_DESCRIPTION[self.parser.fileType][el]['SHORT'])
            actions.append(newAction)
            descActions.append(el)
        action = menu.exec_(self.subframePlotButton.mapToGlobal(point))
        
        if action in actions:
            self.subframePlot(descActions[actions.index(action)])

    def onFramePlotContext(self, point):
        actions = []
        descActions = []

        menu = QMenu(self)
        for el in self.parser.AVAILABLE_PARAMS_TO_PLOT:
            newAction = menu.addAction(self.parser.PARAM_DESCRIPTION[self.parser.fileType][el]['SHORT'])
            actions.append(newAction)
            descActions.append(el)
        action = menu.exec_(self.framePlotButton.mapToGlobal(point))
        
        if action in actions:
            self.framePlot(descActions[actions.index(action)])            

    def cdfTimePlot(self, param='nrb'):
        self.parser.plotCDFTimeDistribution(self.figure, param)
        self.plotCanvas.draw()

    def framePlot(self, param='nrb'):
        self.parser.plotFrameDistribution(self.figure, param)
        self.plotCanvas.draw()

    def subframePlot(self, param='nrb'):
        self.parser.plotSubframeDistribution(self.figure, param)
        self.plotCanvas.draw()

    def savePlot(self):
        filename = QtWidgets.QFileDialog.getSaveFileName()[0]
        if str(filename).strip():
            self.plotCanvas.figure.savefig(filename)

    def handleItemClicked(self, item):
        frameId = -1
        if len(self.table.selectedIndexes()) > 0:
            frameId = self.table.selectedIndexes()[0].row()
        if frameId >= 0:
            self.parser.plotFrame(frameId, self.figure)
            self.plotCanvas.draw()

    def fillTable(self):
        self.csvFn = QtWidgets.QFileDialog.getOpenFileName(None, "Choose CSV file", None, "Comma-separated (*.csv)")[0]
        self.parser.loadFile(self.csvFn)
        self.lblFileInfo.setText("File: " + self.parser.filename + "\n" + 
                                "Entries: " + str(self.parser.linesCountRead) + " of " + str(self.parser.linesCountTotal)+ "\n" + 
                                "Type: " + ("ULSCH" if self.parser.fileType==self.parser.RB_ALLOC_UL else "DLSCH"))
        # set number of rows
        self.table.setRowCount(len(self.parser.parsed))
        for row in self.parser.parsed:
            framesStr = []
            ueStr = []
            for i in self.parser.parsed[row]["subframes"]:
                if not str(i) in framesStr:
                    framesStr.append(str(i))
                for j in self.parser.parsed[row]["subframes"][i]:
                    if not str(j) in ueStr:
                        ueStr.append(str(j))     
            res = [str(self.parser.parsed[row]["frame"]), ",".join(framesStr), ",".join(ueStr), round(self.parser.parsed[row]["nrb_ratio"]*100,2)]
            for el1 in self.col_list:
                item =  QtWidgets.QTableWidgetItem("%s" % res[self.col_list.index(el1)])
                self.table.setItem(row, self.col_list.index(el1), item)
        # if item.checkState() in [QtCore.Qt.Checked, QtCore.Qt.Unchecked] and item.column() == 0:
        #     elId = self.indexDb.index(item.text())
        #     self.tableData[elId]["ENABLED"] = item.checkState()==QtCore.Qt.Checked


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = Window()
    window.resize(800, 600)
    window.show()
    sys.exit(app.exec_())

