"""Tool for enabling/disabling OpenAirInterface T_messsages events

Tested with QT 5.8
"""

__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "01/2018"
__license__ = ""

import sys
import json

from PyQt5 import QtGui, QtCore, QtWidgets

msgGroups = []

def parse_database(filename):
    # /home/epc1/dev/T-tracer/T_messages.txt
    database = []
    indexDatabase = []
    f = open(filename, "r")
    objPos = 0
    for line in f:
        if len(line) > 0:
            line = line.strip()
            if objPos > 0:
                if objPos == 1:
                    objDesc = line[7:]
                    objPos += 1
                elif objPos == 2:
                    objGroup = line[8:]
                    if not objGroup in msgGroups:
                        msgGroups.append(objGroup)
                    objPos += 1
                elif objPos == 3:
                    objPos = 0
                    objFormat = [(f.split(",")[0], f.split(",")[1]) if len(f.split(","))>1 else f for f in ("".join(line[9:].split())).split(":")]
                    database.append({"ID":objId, "DESC":objDesc, "GROUP":objGroup, "FORMAT":objFormat})
                    indexDatabase.append(objId)
            if line[0:2] == "ID":
                objPos = 1
                objId = line[5:]
    f.close()
    return database, indexDatabase

def read_list_enabled(filename, dataDb):
    establishData = json.load(open(filename))
    tableData = {}
    for el in establishData:
        elId = int(el)
        tableData[elId] = dataDb[elId]
        tableData[elId]["ENABLED"] = establishData[el]
    return tableData

class Window(QtWidgets.QWidget):
    col_list = ["ID", "DESC", "GROUP", "FORMAT"]

    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.setWindowTitle(self.tr("T_messages enabling tool"))
        # setup labels
        self.lblTMessagesFn = QtWidgets.QLabel(self.tr("Choose T_messages.txt"))
        self.lblEnabledFn = QtWidgets.QLabel(self.tr("Choose JSON file with enabled status"))
        # setup buttons
        self.saveButton = QtWidgets.QPushButton(self.tr("&Save"))
        self.saveAsButton = QtWidgets.QPushButton(self.tr("Save &as"))
        self.loadButton = QtWidgets.QPushButton(self.tr("&Load"))
        # setup table
        self.table = QtWidgets.QTableWidget(1, len(self.col_list), self)
        for i in range(len(self.col_list)):
            self.table.setHorizontalHeaderItem(i, QtWidgets.QTableWidgetItem(self.col_list[i]));

        # set column width
        self.table.setColumnWidth(0,300)
        self.table.setColumnWidth(1,200)
        self.table.setColumnWidth(2,200)

        # bind actions
        self.table.itemClicked.connect(self.handleItemClicked)
        self.saveButton.pressed.connect(self.saveTable)
        self.saveAsButton.pressed.connect(self.saveAsTable)
        self.loadButton.pressed.connect(self.fillTable)

        # prepare layout
        self.layout = QtWidgets.QGridLayout(self)
        self.layout.addWidget(self.table, 0, 0, 1, 3)
        self.layout.addWidget(self.lblTMessagesFn, 2, 0, 1, 3)
        self.layout.addWidget(self.lblEnabledFn, 3, 0, 1, 3)
        self.layout.addWidget(self.saveButton, 1, 0)
        self.layout.addWidget(self.saveAsButton, 1, 1)
        self.layout.addWidget(self.loadButton, 1, 2)


    def fillTable(self):
        # open T_messages file
        self.tmessagesFn = QtWidgets.QFileDialog.getOpenFileName(None, "Choose T_messages file", None, "T_messages.txt (T_messages.txt);;Text files (*.txt)")[0]
        self.lblTMessagesFn.setText("T_messages.txt: "+self.tmessagesFn)
        # open json status file
        self.jsonFn = QtWidgets.QFileDialog.getOpenFileName(None, "Choose JSON status file", None, "Json files (*.json);;Text files (*.txt)")[0]
        self.lblEnabledFn.setText("Status JSON: "+self.jsonFn)
        # parse T_messages file
        self.dataDb, self.indexDb = parse_database(self.tmessagesFn)
        # parse json status file
        self.tableData = read_list_enabled(self.jsonFn, self.dataDb)
        # set number of rows
        self.table.setRowCount(len(self.tableData))

        # fill table
        for row in range(len(self.tableData)):
            for el in self.col_list:
                item =  QtWidgets.QTableWidgetItem('%s' % self.tableData[row][el])
                if self.col_list.index(el) == 0:
                    item.setFlags(QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsEnabled | (not QtCore.Qt.ItemIsEditable))
                    item.setCheckState(QtCore.Qt.Checked if self.tableData[row]["ENABLED"] else QtCore.Qt.Unchecked)
                else:
                    item.setFlags(item.flags() ^ QtCore.Qt.ItemIsEditable)
                self.table.setItem(row, self.col_list.index(el), item)

    def saveTable(self):
        btsEstablishedDict = {}
        filename = self.jsonFn
        if len(self.dataDb) <= 0:
            return False
        for el in self.tableData:
            btsEstablishedDict[el] = self.tableData[el]["ENABLED"]
        with open(filename, 'w') as outfile:
            json.dump(btsEstablishedDict, outfile)

    def saveAsTable(self):
        btsEstablishedDict = {}
        filename = QtWidgets.QFileDialog.getSaveFileName()[0]
        if len(self.dataDb) <= 0:
            return False
        for el in self.tableData:
            btsEstablishedDict[el] = self.tableData[el]["ENABLED"]
        with open(filename, 'w') as outfile:
            json.dump(btsEstablishedDict, outfile)

    def handleItemClicked(self, item):
        if item.checkState() in [QtCore.Qt.Checked, QtCore.Qt.Unchecked] and item.column() == 0:
            elId = self.indexDb.index(item.text())
            self.tableData[elId]["ENABLED"] = item.checkState()==QtCore.Qt.Checked
            # print('"%s" %s' % (item.text(), "Checked" if item.checkState()==QtCore.Qt.Checked else "Unchecked"))

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    window = Window()
    window.resize(800, 600)
    window.show()
    sys.exit(app.exec_())
