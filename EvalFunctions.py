# -*- coding: utf-8 -*-
""" Module contains common evaluation functions

Special for evaluating LTE system and some other common evaluation methods
"""
import numpy as np
import csv, os
import mmap
# import for unpacking timespec struct
from struct import unpack
from datetime import datetime

__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "01/2018"
__license__ = ""



def unpackRiv(RIV, N_RB_UL):
    """Unpack RBstart and Lcrb from RIV value according to 3GPP TS 36.213-8.1.1
    
    Arguments:
        RIV {integer} -- given RIV value
        N_RB_UL {integer} -- number of resource blocks for uplink
    
    Returns:
        (integer, integer, integer) -- position of the start RB, length of RBs group, position of the last RB
    """
    # define equations from 3GPP TS 36.213-8.1.1
    eq1 = lambda x, y: N_RB_UL * (x - 1) + y
    eq2 = lambda x, y: N_RB_UL * (N_RB_UL - x + 1) + (N_RB_UL - 1 - y)
    # init result values
    RBstart = -1
    Lcrb = 0
    # i - RBstart, j - Lcrb
    # try all options, solving two variables from one equation
    for i in range(N_RB_UL):
        for j in range(1,N_RB_UL+1-i):
            if eq1(j,i)==RIV or eq2(j,i)==RIV:
                RBstart, Lcrb = i, j
    # calculate the last RB position
    RBStop = RBstart + Lcrb - 1
    return RBstart, Lcrb, RBStop

def rivToBitmapArray(RIV, N_RB_UL):
    """Unpack RIV and convert to list showing position of allocated RBs according to given RIV
    
    Arguments:
        RIV {integer} -- given RIV value
        N_RB_UL {integer} -- number of resource blocks for uplink
    """
    # unpack RIV
    RBstart, _, RBStop = unpackRiv(RIV, N_RB_UL)
    bitMap = [int(i >= RBstart and i <= RBStop) for i in range(N_RB_UL)]
    return  bitMap

def appendToCSV(values, csvFile="test.csv"):
    """Append list entries to the CSV file
    
    Arguments:
        values {list} -- values to add
    """
    with open(csvFile, 'a') as csvfile:
        spamwriter = csv.writer(csvfile, quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(values)

def getCpuFreqMHz():
    """Get CPU frequency in MHz, works only on Linux
    """
    h = os.popen("cat /proc/cpuinfo | grep MHz")
    return float(h.readlines()[0].split(":")[1].strip())

def formatTimestampToString(ts, fmt= '%Y-%m-%d %H:%M:%S.%f'):
    """Format timestamp to string

    Arguments:
        ts {float} -- timestamp value
        fmt {string} -- format for the output

    Returns:
        string -- formatted string or empty if exception occured
    """
    try:
        return datetime.fromtimestamp(ts).strftime(fmt)
    except:
        return ""

def unpackTimespec(byteArr):
    """Unpacks C struct timespec from bytes defined as (long long, long)"""
    try:
        tStruct = unpack('ql', byteArr)
        timestampObj = (10e8 * int(tStruct[0]) + tStruct[1])/10e8
        return timestampObj
    except:
        return 0 

def convertIntToBinIntArray(sbinInt, length):
    """Convert integer var to a list of bin of defined length
    
    Arguments:
        sbinInt {integer} -- integer which has to be converted
        length {integer} -- final length of a list

    Returns:
        list -- list contains int converted to bin
    """
    inStr = str(bin(sbinInt))[2:]
    inStr = "0"*(length - len(inStr)) + inStr
    binArr = [int(inStr[i]) for i in range(length)] 
    return binArr

def calcCdfOrCcdf(values, isCcdf=False):
    """Calculate CDF or CCDF for a given list
    
    Arguments:
        values {list} -- values to calculate CDF/CCDF
    
    Keyword Arguments:
        isCcdf {boolean} -- calculate CCDF if true otherwise CDF (default: {False})
    
    Returns:
        (list, list) -- x axis values (parameter), y axis values (CDF)
    """
    # convert to a numpy array
    values = np.array(values)
    # sort values
    values = np.sort(values)
    # normalize values to the sum of all
    valuesNorm = values/ sum(values)
    # calc the cumulative sum
    cY = 1 - np.cumsum(valuesNorm) if isCcdf else np.cumsum(valuesNorm)
    return values, cY

def calcDiffValues(values, sortBefore = False):
    """Calcaulte differences between each two values
    
    Arguments:
        values {list} -- list of values to calculate differences
    
    Keyword Arguments:
        sortBefore {boolean} -- sort the input list before calculation (default: {False})

    Returns: 
        list -- differences between each two values
    """
    # convert to ms
    values = np.sort(np.array(values)) if sortBefore else  np.array(values)
    diffs  = values[1:] - values[:-1]
    return diffs

def tryMapValue(val, mappingFunc):
    """Tries to map a value with a given function
    
    Arguments:
        val {object} -- any given value is to be mapped
        mappingFunc {function} -- mapping method is to be used
    
    Returns:
        object -- None if couldn't map otherwise mapped value
    """
    try:
        return mappingFunc(val)
    except ValueError:
        return None

def linesCount(filename):
    """Count number of lines in the file
    
    Arguments:
        filename {string} -- filepath
    
    Returns:
        integer -- number of lines in the file
    """
    non_blank_count = 0

    with open(filename) as infp:
        for line in infp:
            if line.strip():
                non_blank_count += 1
    return non_blank_count    

def readCSVFile(filename, nValues=50000, header=[], defaultMapFunc=None, mappingFuncs=[]):
    """Read a CSV file, try to auto-detect a header
    
    Arguments:
        filename {string} -- filepath to the CSV file
    
    Keyword Arguments:
        nValues {integer} -- number of values to be read (default: {50000})
    """
    # function tries to determine a header in a first line by using mapping 
    # or inconsistence of a given header to the file row
    # TODO: find a smarter way to determine a header, maybe compare first several lines and determine how different the first line is
    
    # init result list
    result = []
    with open(filename, 'r') as csvfile:
        # initialize reader
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        # initialize values
        i = 0
        tmpHeader = []
        isHeader = False
        for row in spamreader:
            # if reached the limit
            if i == nValues:
                break
            # initialize values for each row
            resultRow = {}
            tmpRow = {}
            for j in range(len(row)):
                # use map func from the list of default if not defined
                mapFunc = mappingFuncs[j] if j < len(mappingFuncs) else defaultMapFunc
                # try to map value with a function
                mappedValue = tryMapValue(row[j], mapFunc) if not mapFunc is None else row[j]
                # if the first line checking for the header
                if i == 0:
                    # if the mapping was unsuccesfull, assume this is a header
                    if mappedValue is None:
                        isHeader = True
                    # otherwise store as a normal value
                    elif not isHeader and j < len(header):
                        tmpRow[header[j]] = mappedValue
                    # store to a temp header anyway as we don't know the result for the next values
                    tmpHeader.append(row[j])
                else:
                    if mappedValue is None:
                        raise ValueError("Mapping failed, probably wrong CSV format")
                    resultRow[header[j]] = mappedValue
            # if header found on the first line
            if i == 0 and isHeader:
                header = tmpHeader
            # if first line but the header
            elif i == 0:
                resultRow = tmpRow

            # if not a first line or not a header
            if i != 0 or not isHeader:
                result.append(resultRow)
            i += 1
    return result, header