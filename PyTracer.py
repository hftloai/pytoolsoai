"""Python with PyQT implementation of T_Tracer for OpenAirInterface5G project [build of 08/08/2017]

Tested with QT 5.8
"""

__author__ = "Igor Kim"
__credits__ = ["Igor Kim"]
__maintainer__ = "Igor Kim"
__email__ = "igor.skh@gmail.com"
__status__ = "Development"
__date__ = "01/2018"
__license__ = ""

# import python dependencies
import sys

# import local dependencies
from OAITracer import OAITracer

# imports QT dependencies
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot, QRunnable, QSize, QThreadPool, QTimer
from PyQt5.QtWidgets import QApplication, QGridLayout, QWidget, QLabel, QPushButton, QTextEdit
from PyQt5.QtChart import QChart, QChartView, QLineSeries, QValueAxis
from PyQt5.QtGui import QTextCursor, QPainter, QColor

class WorkerSignals(QObject):
    '''
    Defines the signals available from a running worker thread.

    Supported signals are:
    '''
    updateConnectionInfo = pyqtSignal()
    addToLog = pyqtSignal(str)
    updateRuntimePlot = pyqtSignal(object)
    updatePlot = pyqtSignal(object)

class Worker(QRunnable):
    '''
    Worker thread

    :param args: Arguments to make available to the run code
    :param kwargs: Keywords arguments to make available to the run code

    '''
    def __init__(self, tracerProcess):
        super(Worker, self).__init__()
        self.tracerProcess = tracerProcess
        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        self.tracerProcess.mainLoop(self.signals)

class Window(QWidget):
    def __init__(self, parent = None):
        QWidget.__init__(self, parent)

        # initialize tracer object
        self.tracerProcess = OAITracer()

        # prepare GUI objects
        self.connectButton = QPushButton(self.tr("&Connect"))
        self.lblConnectionStatus = QLabel(self.tr("Not connected"))
        self.updateConnectionInfo()
        self.logTextEdit = QTextEdit()
        # prepare GUI graph
        self.chart = QChart()
        self.chart.legend().hide()
        self.view = QChartView(self.chart)
        self.view.setRenderHint(QPainter.Antialiasing)
        self.maxY = 0.004
        self.maxX = 9
        # set main curve
        self.curve = QLineSeries()
        pen = self.curve.pen()
        pen.setWidthF(.1)
        self.curve.setPen(pen)
        self.curve.setUseOpenGL(True)
        self.chart.addSeries(self.curve)

        # set avg curve
        self.curveAvg = QLineSeries()
        pen = self.curve.pen()
        pen.setWidthF(.1)
        pen.setBrush(QColor(255, 0, 0, 127))
        self.curveAvg.setPen(pen)
        self.curveAvg.setUseOpenGL(True)
        self.chart.addSeries(self.curveAvg)

        # draw layout
        layout = QGridLayout()
        layout.addWidget(self.lblConnectionStatus, 0, 0, 1 ,2)
        layout.addWidget(self.connectButton, 1, 0,1,2)
        layout.addWidget(self.logTextEdit, 2, 0, 1, 2)
        layout.addWidget(self.view, 3, 0, 1, 2)
        self.setLayout(layout)
        self.setWindowTitle(self.tr("PyTracer for OAI5g"))
        self.resize(600, 600)

        # bind events
        self.connectButton.pressed.connect(self.connect)
        # Run thread
        self.threadpool = QThreadPool()

    def addToLog(self, log):
        if len(log) > 0:
            self.logTextEdit.moveCursor(QTextCursor.Start, 0)
            self.logTextEdit.insertPlainText(log + "\n")

    def updateConnectionInfo(self):
        if not self.tracerProcess.connected:
            if self.tracerProcess.attempts > 0:
                self.lblConnectionStatus.setText("Connection refused (%d of %d) repeat in %d s" % (self.tracerProcess.attempts, self.tracerProcess.maxAttempts, self.tracerProcess.tRepeatConnection))
            if self.tracerProcess.attempts >= self.tracerProcess.maxAttempts:
                self.lblConnectionStatus.setText("Unable to connect, try again later")
        else:
            self.lblConnectionStatus.setText("Connected to %s on port %s"% (self.tracerProcess.serverAddress[0], self.tracerProcess.serverAddress[1]))

    def updateRuntimePlot(self, data):
        """  """
        dataTmp = data
        # check max Y value
        if self.maxY < max(dataTmp["Y"]):
            self.maxY = max(dataTmp["Y"])
        # check max X value
        if self.maxX < max(dataTmp["X"]):
            self.maxX = max(dataTmp["X"])
        self.minX = min(dataTmp["X"])
        # clear old data curves
        self.curve.clear()
        self.curveAvg.clear()
        # calc the buffer len
        dataLen = len(dataTmp["Y"])
        # calc the average value over the Y
        avgVal = sum(dataTmp["Y"]) / dataLen
        # add values to the plot
        for i in range(dataLen):
            # plot the graph
            self.curve.append(dataTmp["X"][i], dataTmp["Y"][i])
            # plot the avg
            self.curveAvg.append(dataTmp["X"][i], avgVal)

        # init axis
        axisX = QValueAxis()
        axisY = QValueAxis()
        # set axis range
        axisX.setRange(self.minX , self.maxX)
        axisY.setRange(0, self.maxY)
        # set axis titles
        axisX.setTitleText("Function execution")
        axisY.setTitleText("Execution time, ms")
        # set number of ticks
        axisX.setTickCount(10)
        axisY.setTickCount(5)
        # apply curves
        self.chart.setAxisX(axisX, self.curve)
        self.chart.setAxisY(axisY, self.curve)
        self.chart.setAxisX(axisX, self.curveAvg)
        self.chart.setAxisY(axisY, self.curveAvg)

    def connect(self):
        # Pass the function to execute
        worker = Worker(self.tracerProcess) # Any other args, kwargs are passed to the run function
        # bind signaling
        worker.signals.updateConnectionInfo.connect(self.updateConnectionInfo)
        worker.signals.addToLog.connect(self.addToLog)
        worker.signals.updateRuntimePlot.connect(self.updateRuntimePlot)
        # Execute
        self.threadpool.start(worker)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = Window()
    window.show()
    sys.exit(app.exec_())
