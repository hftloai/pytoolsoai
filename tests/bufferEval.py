import csv
import matplotlib.pyplot as plt

def readFile(filename="../meas/assign_rbs_1ue.csv", nValues=30000):
    timeId = []
    ueId = []
    bufferSize = []
    with open(filename, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        i = 0
        for row in spamreader:
            if i == nValues:
                break
            i += 1
            timeId.append(float(row[0]))
            ueId.append(int(row[2]))
            bufferSize.append(float(row[3]))
    return timeId, ueId, bufferSize

timeId, ueId, bufferSize = readFile()
xValues = []
yValues = []
xValues1 = []
yValues1 = []
for i in range(len(timeId)):
    if ueId[i] == 0:
        xValues.append(timeId[i])
        yValues.append(bufferSize[i])
    if ueId[i] == 1:
        xValues1.append(timeId[i])
        yValues1.append(bufferSize[i])

plt.plot(xValues, yValues, xValues1, yValues1)
plt.show()
