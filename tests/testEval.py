"""
Igor Kim [igor.skh@gmail.com]

2018/01
"""


import os
import sys

sys.path.insert(0, '../')
import eval

# inputs
OUTPUT_PATH = 'plots/'
N_SAMPLES = 15000
SAVE_FIG = False
FUNC_NAME = 'CDF_TEMP'
FIG_EXT = '.png'
FILES_TO_EVAl = ["meas/sss.csv"]
PLOT_CCDF = False

if __name__ == "__main__":
    for f in FILES_TO_EVAl:
        ts, values = eval.readFile(f, N_SAMPLES)
        values = eval.calcDiffValues(ts)
        #plt = eval.plotLine(values, 'Times')
        plt = eval.plotCdf(values, os.path.splitext(os.path.basename(f))[0], PLOT_CCDF)
        #plt.xlabel(u'Call interval [ms]')

    # set limits
    # plt.xlim(0, 20)
    # plt.ylim(0, 20)
    # plt.xscale('log')
    # plt.yscale('log')

    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=2, mode="expand", borderaxespad=0.)
    # save if path is given
    if SAVE_FIG:
        plt.savefig(OUTPUT_PATH + FUNC_NAME + '_' + str(N_SAMPLES) + FIG_EXT)
    else:
        plt.show()